from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt 
import json 
from .models import Connection, ChatMessage
import boto3
from django.core import serializers


# Create your views here.
@csrf_exempt
def test(request): 
     return JsonResponse({"message": "hello Daud"}, status=200)

def _parse_body(body):
    body_unicode = body.decode("utf-8") 
    return json.loads(body_unicode) 

@csrf_exempt
def connect(request): 
    body = _parse_body(request.body)
    connection_id = body['connectionId'] 
    Connection.objects.create(connection_id=connection_id)

    return JsonResponse({'message': 'connect successfully'}, status=200)

@csrf_exempt
def disconnect(request):
    body = _parse_body(request.body)
    connection_id = body['connectionId'] 
    Connection.objects.get(connection_id=connection_id).delete()

    return JsonResponse({'message': 'disconnect successfully'}, status=200)

def _send_to_connection(connection_id, data):
    gatewayapi = boto3.client('apigatewaymanagementapi', endpoint_url="",
    region_name="us-east-1", aws_access_key_id="", aws_secret_access_key="")

    return gatewayapi.post_to_connection(ConnectionId=connection_id, Data=json.dumps(data).encode('utf-8')) 

# @csrf_exempt
# def send_message(request):
#     body = _parse_body( request.body )
#     chat_message = ChatMessage.objects.create( username=body['body']['username'],
#     message=body['body']['content'], timestamp=body['body']['timestamp'])
#     connections = [i.connection_id for i in Connection.objects.all()]
#     body={'username': chat_message.username, 'message':chat_message.message, 
#     'timestamp':chat_message.timestamp}
#     data = {'messages':[body]}
#     for connection in connections:
#         _send_to_connection(connection, data)
#     return JsonResponse('successfully sent', status=200, safe=False)

    

# @csrf_exempt
# def get_messages(request):
#     body = _parse_body( request.body )
#     messages = [i for i in ChatMessage.objects.all()]
#     data = serializers.serialize("json",messages)

#     _send_to_connection(body["connectionId"], data)
#     return JsonResponse({'message': data })


@csrf_exempt
def send_message(request):
    body = _parse_body(request.body)['body']
    message = ChatMessage(username=body['username'], message=body['content'], timestamp=body['timestamp'])
    message.save()
    connections = Connection.objects.all()
    data = {'messages': [body]}

    for connected in connections:
        _send_to_connection(connected.connection_id, data)

    return HttpResponse('done', status=200)


@csrf_exempt
def get_messages(request):
    messages = ChatMessage.objects.order_by('-id')
    chats = []

    for message in messages:
        chats.append({
            'username': message.username,
            'message': message.message,
            'timestamp': message.timestamp })

    data = {'messages': chats}

    body = _parse_body(request.body)
    connection_id = body['connectionId']
    connection = Connection.objects.get(connection_id=connection_id)
    _send_to_connection(connection.connection_id, data)
