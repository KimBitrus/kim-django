import os
from setuptools import setup, find_packages

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(

    name = 'django-bitruschoji4ralscrumy',
    version = '0.1',
    packages = find_packages(),
    include_packages_data = True,
    description = 'A Django app at linuxjobber django internship',
    long_description_content_type ='text/x-rst',
    long_description =  README,
    url = 'https://www.example.com/',
    author = 'Bitrus Kim Dung Choji',
    author_email ='bitruschoji4real@gmail.com',
    license = 'BSD-3-Clause', # Example license
    classifiers = [
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: X.Y',  # Replace "X.Y" as appropriate
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ], 
    python_requires='>=3.6',
)