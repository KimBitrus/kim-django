from django.contrib import admin
from .models import ScrumyGoals, GoalStatus, ScrumyHistory
# Register your models here.



class ScrumyGoalsAdmin(admin.ModelAdmin):

    pass


class GoalStatusAdmin(admin.ModelAdmin):

    pass

class ScrumyHistoryAdmin(admin.ModelAdmin):

    pass

admin.site.register(ScrumyGoals, ScrumyGoalsAdmin)

admin.site.register(ScrumyHistory, ScrumyHistoryAdmin)
admin.site.register(GoalStatus, GoalStatusAdmin)

