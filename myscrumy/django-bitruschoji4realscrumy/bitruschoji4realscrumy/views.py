from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import  ScrumyGoals, GoalStatus
from django.contrib.auth.models import User
import random
from .forms import SignupForm, CreateGoalForm, MoveGoalForm
from django.contrib.auth.models import Group
from django.contrib.auth.hashers import make_password
from django.contrib import messages
from django.contrib.auth import logout


# Create your views here.
def index(request): 
    # goal = ScrumyGoals.objects.filter(goal_name="Learn Django")
    # return HttpResponse(goal)
    form = SignupForm()
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.password = make_password(form.cleaned_data['password'])
            new_user.save()
        # return home(request)

            # developer = Group.objects.get(name="Developer")
            # new_user = User.objects.get(username=form.cleaned_data["username"])
            # developer.user_set.add(new_user)
            messages.success(request, "Account has been created successfully")
            return redirect("/bitruschoji4realscrumy/accounts/login", permanent=True)

    else:
        form = SignupForm()

    dictionary = {"form":form}
    return render(request, "bitruschoji4realscrumy/index.html", dictionary)



def move_goal(request, goal_id):
    
    try:
        goal = ScrumyGoals.objects.get(goal_id=goal_id)
        
    except Exception as e: 
    
        return render(request, "bitruschoji4realscrumy/exception.html", {"error":"A record with that goal id does not exist"})
    current_user = request.user
    if request.method == 'POST':
        form = MoveGoalForm(request.POST)
        if form.is_valid():

            if current_user.groups.filter(name="Developer").exists():
                if form.cleaned_data['status_name'] == "Done Goal":
                    messages.error(request, "you don't have permmission to perform this action.")
                    return redirect('bitruschoji4realscrumy:home', permanent=True)
                if goal.user != current_user:
                    messages.error(request, """you don't have permmission to perform this action.
                    Try moving a goal you created""")
                    return redirect('bitruschoji4realscrumy:home', permanent=True)

            if current_user.groups.filter(name="Quality Assurance").exists():
                if form.cleaned_data['status_name'] == "Weekly Goal":
                    messages.error(request, "you don't have permmission to perform this action.")
                    return redirect('bitruschoji4realscrumy:home', permanent=True)
                if goal.user != current_user and goal.goal_status.status_name != "Verify Goal":
                    messages.error(request, "You can only move goals of others provided they have been verified")
                    return redirect('bitruschoji4realscrumy:home', permanent=True)

            if current_user.groups.filter(name="Owner").exists():
                if goal.user != current_user:
                    messages.error(request, """you don't have permmission to perform this action.
                    Try moving a goal you created""")
                    return redirect('bitruschoji4realscrumy:home', permanent=True)

            goal.goal_status = GoalStatus.objects.get(status_name=form.cleaned_data['status_name'])
            goal.save()
            messages.success(request, f"The goal has been sucessfully moved to {form.cleaned_data['status_name']}")
            return redirect('bitruschoji4realscrumy:home', permanent=True)

    else:
        #return HttpResponse(goal.goal_name)
        form = MoveGoalForm()
    return render(request, 'bitruschoji4realscrumy/movegoal.html', {'form':form})


def add_goal(request):
    # goal= GoalStatus.objects.get(status_name= 'Daily Goal')
    # myuser = User.objects.get(username='kim')
    # data = ScrumyGoals.objects.all()
    # for new_loop in data:
    #     random_id = random.randint(1000, 9999)
    #     if new_loop.goal_id != random_id:
    #         new_scrumy_goal = ScrumyGoals(goal_name ='Keep Learning Django',goal_id = random_id,
    #         created_by='kim', moved_by='kim', owner='kim', goal_status = goal,user=myuser)
    #         new_scrumy_goal.save()
    #         #return HttpResponse("Created")

    current_user = request.user
    if request.method == "POST":
        form = CreateGoalForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            if current_user.groups.first().name in ["Developer", "Quality Assurance", "Owner"]:
                if form.cleaned_data['user'] != current_user or \
                    form.cleaned_data['goal_status'].status_name != "Weekly Goal":
                    messages.error(request, "You can only create a weekly goal for yourself")
                    return render(request, 'addgoal.html', {'form': form})
            form.save()
            messages.success(request, "Goal has been successfully created")
            return redirect('bitruschoji4realscrumy:home', permanent=True)
    else:
        form = CreateGoalForm()

    return render(request, 'bitruschoji4realscrumy/addgoal.html', {'form': form})


def home(request):  
    # goal = ScrumyGoals.objects.get(goal_name='Learn Django')
    # dictionary = {"goal_name":goal.goal_name, "goal_id":goal.goal_id, "user":goal.user}    
    # return render(request, "bitruschoji4realscrumy/home.html", dictionary)
    # users = User.objects.all()
    # done_goal = GoalStatus.objects.get(status_name="Done  Goal").goals.all()
    # weekly_goal = GoalStatus.objects.get(status_name="Weekly Goal").goals.all()
    # daily_goal = GoalStatus.objects.get(status_name="Daily Goal").goals.all()
    # verify_goal = GoalStatus.objects.get(status_name="Verify Goal").goals.all()
    # dictionary = {
    # 'users':users,
    # 'weekly_goal': weekly_goal,
    # 'daily_goal': daily_goal,
    # 'verify_goal': verify_goal,
    # 'done_goal': done_goal,
    #}
    return render(request, "bitruschoji4realscrumy/home.html")


def logout_view(request):
    logout(request)
    redirect('bitruschoji4realscrumy:login', permanent=True)

