from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class ScrumyGoals(models.Model):
    # class Meta:
    #     verbose_name_plural = "Scrumy Goals"
    goal_name = models.CharField(max_length=300)
    goal_id = models.IntegerField()
    created_by = models.CharField(max_length=300)
    moved_by = models.CharField(max_length=300)
    owner = models.CharField(max_length=300)
    goal_status=models.ForeignKey(to='GoalStatus', related_name="goals", on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE,related_name="users")

    def __str__(self):
        return self.goal_name
    

class ScrumyHistory(models.Model):
    # class Meta:
    #     verbose_name_plural = "Scrumy History"
    moved_by = models.CharField(max_length=300)
    created_by = models.CharField(max_length=300)
    moved_from = models.CharField(max_length=300)
    moved_to = models.CharField(max_length=300)
    time_of_action = models.DateTimeField()
    goal = models.ForeignKey(ScrumyGoals, on_delete=models.CASCADE) 
    
    def __str__(self):
        return self.created_by

class GoalStatus(models.Model):
    # class Meta:
    #     verbose_name_plural = "GoalStatus"
    status_name = models.CharField(max_length=300)

    def __str__(self):
        return self.status_name
