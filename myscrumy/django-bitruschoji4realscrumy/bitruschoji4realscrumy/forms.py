from django.forms import ModelForm, Form
from .models import User, GoalStatus, ScrumyGoals, ScrumyHistory
from django import forms

class SignupForm(ModelForm):
    class Meta:
        model = User
        fields = ["first_name","last_name","email","username","password"]
    
class CreateGoalForm(ModelForm):
    class Meta:
        model = ScrumyGoals
        fields = ["goal_name", "user"]


class MoveGoalForm(Form):
    choices = (('Daily Goal', 'Daily Goal'), ('Weekly Goal', 'Weekly Goal'), ('Done Goal', 'Done Goal'), ('Verify Goal', 'Verify Goal'))
    status_name = forms.ChoiceField(choices=choices)
    

